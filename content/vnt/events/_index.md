+++
title = "Eventos da empresa"
description = ""
weight = 1
+++

## Mensal

### Alinhamento geral

Uma reunião onde são apresentados os resultados do mês que passou e o planejamento para o mês que está começando. A ideia é deixar todo mundo sempre atualizado sobre o andamento das empresas e também ter um espaço para a participação de todos nas decisões sobre o futuro. A participação ativa do time nessas reuniões é de extrema importância para que as empresas caminhem para um ponto que faça sentido para todos.

Estrutura da reunião (1h de duração):

* Apresentação dos resultados das metas do mês passado
* Apresentação das metas do mês atual
* Espaço para dúvidas e participação do time

## Trimestral

### Apresentação de resultados e metas

Uma reunião parecida com a mensal porem com dados financeiros consolidados do trimestre. Um reunião mais longa onde serão discutidos todos os erros e acertos do trimestre. Nessa mesma reunião o time irá propor junto as prioridades para o próximo trimestre.