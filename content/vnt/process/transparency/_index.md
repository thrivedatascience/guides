+++
title = "Transparência"
description = ""
weight = 1
+++

## Documento comercial

No inicio de cada projeto o comercial deve sempre entregar para o líder do squad um documento detalhando todos os termos do contrato assinado com o cliente.

Informações obrigatórias do documento:

* Prazo
* Escopo
    * Explicar detalhadamente quais são as expectativas do cliente

* Contatos do cliente
    * Product Owner
    * Sponsor
    * Developers
