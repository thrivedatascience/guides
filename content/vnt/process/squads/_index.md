+++
title = "Squads"
description = ""
weight = 1
+++

## O que são squads?

São times de no máximo 10 pessoas que atendem um determinado número de clientes da empresa. Dentro de cada squad existem pessoas com responsabilidades distintas para que os clientes sejam atendidos da melhor maneira possível.

## Cargos em um squad

* **Líder de projeto:** Responsável por ser o elo entre o cliente e a nossa operação, pessoa no time com o maior conhecimento sobre o projeto. Deve guiar o dia a dia de trabalho do produto, delegar tarefas para outros membros do squad e priorizar demandas com o cliente.
* **Especialistas:** Desenvolvedores ou designers que trabalham em um ou mais projetos dentro do squad. Estão sempre disponíveis para ajudar os líderes.
* **Líder do squad:** Responsável por manter o squad sempre funcionando. Deve ser a pessoa que resolve os impeditivos de todos os projetos para que seus líderes e especialistas possam focar apenas em produzir.

## Rotina de um squad

* **Segunda:** Líderes de projeto executam as plannings com seus clientes
* **Terça:** Pela manhã todos do time fazem uma planning para definir como será a alocação da semana.
* **Sexta:** Reuniões de entrega para os clientes