+++
title = "Processos"
weight = 1
+++

## Pessoas

* [Squads]({{%relref "squads" %}})

## Projetos

* [Processos do XP]({{%relref "xp" %}})
* [Transparência]({{%relref "transparency" %}})

