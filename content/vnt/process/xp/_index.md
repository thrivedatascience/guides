+++
title = "Processos do XP"
description = ""
weight = 1
+++

Processos de uso obrigatório em todos os projetos:

* **Planning:** Toda iteração de desenvolvimento deve começar com uma reunião de planejamento.
* **Daily:** Todo dia de trabalho de iniciar com uma mensagem explicando sua situação no projeto.
* **Delivery:** Toda iteração deve terminar com uma reunião de entrega para o cliente, nessa reunião o time vai apresentar tudo o que foi desenvolvido compartilhando a tela e navegando no sistema.
* **Retrospective:** Logo após a Delivery o time deve se reunir e executar uma cerimônia de retrospectiva sobre a iteração que passou.
* **User stories:** Todo projeto deve ter user stories definidas e validadas com o cliente.