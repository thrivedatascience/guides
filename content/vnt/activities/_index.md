+++
title = "Tipos de atividade"
description = ""
weight = 1
+++

## Interno

### Estudos

Cada membro do time tem a possibilidade de alocar tempo para estudar o que bem entender durante períodos em que não está trabalhando em nenhum projeto

### Investimento

Quando uma pessoa está trabalhando em um produto interno essa alocação é contabilizada como um investimento. Tempo gasto com produção de palestras e participação em eventos também devem ser considerados investimento.

### Administrativo

Esse tipo de alocação é executado na maioria das vezes pelos cargos de gerência e diretores. São alocadas aqui as funções administrativas do negócio.

### Recrutamento

Todos os envolvidos em recrutamento devem alocar seu tempo nesse tipo de atividade.

## Clientes

### Projetos

Todo tempo gasto com tarefas para clientes deve ser alocado no seu respectivo projeto no Everhour.