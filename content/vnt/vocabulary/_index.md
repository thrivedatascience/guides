+++
title = "Vocabulário"
description = ""
weight = 10
+++

* **Product Owner (PO)**: É o líder responsável por maximizar o valor do produto. Também tem a responsabilidade de cuidar da estratégia de negócio, design do produto, analise de mercado, gerência do produto e ligação com o cliente.
  
* **Product Manager (PM)**: É responsável pelo desenvolvimento de produtos para uma organização. É quem gerencia as tarefas que serão feitas ao longo do desenvolvimento do produto.

* **Planning**: Reunião semanal em que o time monta a _sprint_ priorizando os items que serão desenvolvidos.

* **Sprint**: Intervalo de tempo em que são desenvolvidos os items que foram priorizados na _planning_.

* **Daily**: Reunião diária em que o time se junta para falar sobre o que foi feito no dia anterior, o que será feito no dia e se possui algum impeditivo. Geralmente essas reuniões são feitas de pé e devem durar o mínimo possível afim de não atrapalhar no desenvolvimento do projeto.

* **Retro**: Reunião para fazer uma retrospectiva do projeto, o que deu certo, o que não deu.
  
* **Backlog**: Lista de itens que são priorizados para o time de desenvolvimento afim de realizar uma entrega. Esta lista pode ser implementa com uma planilha, bloco de notas ou com alguma ferramenta como Trello.
  
* **Backlog Item**: Uma tarefa que está no _backlog_. Cada _backlog item_ deve ter no mínimo um título, detalhes, estimativa, e tipo. Tipos de _backlog item_ podem incluir:
  
  * **User Story**: Representa uma característica ou melhoria no produto que podem mudar a experiência do usuário final.
  * **Points**: Numero que representa o quanto de esforço é demandado para aquela tarefa. Geralmente é usado o parametro de **hora** para medir o esforço. Usamo um dos seguintes número da série de fibonacci para representar o esforço em horas: 0,1, 1, 2, 3, 5, 8, 13, 21. Quando um item tem um valor de esforço que não pode ser mensurado em horas, atribuímos o valor de 40.
  *  **Feature**: Quando queremos adicionar uma nova funcionalidade ao produto.
  *  **Bug**: Igual a descrição acima, mas resulta em uma correção de um comportamento inesperado/indesejado.
  *  **Chore**: Um item que não resulta em uma alteração que possa ser percebida pelo usuário ou pelo _product owner_. Exemplo: Integração com algum sistema de log de erros, implementar algum backup, etc.

