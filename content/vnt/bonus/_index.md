+++
title = "Bônus"
description = ""
weight = 6
+++

## Variáveis

* TV - total vendido: Número de horas trabalhadas vezes o valor cobrado do cliente por cada hora. Ex: Trabalhou 100h para um cliente que cobramos R$50/h o TV=R$500,00.
* LPH - Lucro por hora: Porcentagem de lucro por hora que a empresa teve em cada projeto trabalhado.
* COM - comissão: a porcentagem de comissão é definida de acordo com a responsabilidade da pessoa.
* Buffer: Valor referente ao montante necessário para arcar com todos os custos da empresa (trabalhista, fiscal e fixo). O valor do buffer é dividido em níveis.
  * 0: Menos de um mês de custo em caixa
  * 1: 2 meses de custo em caixa
  * 2: 4 meses de custo em caixa
  * 3: 6 ou mais meses de custo em caixa

## Cálculo do bônus

TV * LPH * COM * Buffer

## Exemplo

Uma pessoa trabalhou 500h no trimestre em um mesmo projeto onde a empresa cobrou R$75/h e teve um lucro de 30%. O buffer do trimestre fechou no nível 2 e a comissão para o cargo dessa pessoa é de 5%.

R$37.500 * 0.3 * 0.02 * 2 = R$1.125 de bônus no trimestre.