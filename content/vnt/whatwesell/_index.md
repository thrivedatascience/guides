+++
title = "O que vendemos"
description = ""
weight = 3
+++

## Nave

Consultoria e desenvolvimento de software.

* Desenvolvimento de produto completo

    * TBD


* Outsourcing de programação

    * TBD


* Consultoria

    * TBD


## Thrive

Consultoria e produtos de Data Science.

* Consultoria
  * TBD

* Produto predição de demanda varejo
  * TBD