+++
title = "Frontend"
description = ""
weight = 1
+++

Abaixo os pacotes que damos preferência na escolha quando estamos desenvolvendo.

### UI

* [Material-Ui](https://material-ui.com/)
* [Rebass](https://rebassjs.org/)
* [Ant Design](https://github.com/ant-design/ant-design/)
* [Semantic UI React](https://github.com/Semantic-Org/Semantic-UI-React)
* [Gestalt (Pinterest)](https://github.com/pinterest/gestalt)
* [Grommet](https://github.com/grommet/grommet)
* [React Native Paper](https://callstack.github.io/react-native-paper/index.html)
* [Elements](https://elemefe.github.io/element-react/#/en-US/quick-start)

### Animação

* [React Spring](http://react-spring.surge.sh/)
* [Pose](https://popmotion.io/pose/)
  
### Utilitários

* [Redux Form](https://github.com/erikras/redux-form/)
* [Formik](https://github.com/jaredpalmer/formik)

### Server side rendering

* [Next.js](https://github.com/zeit/next.js/)
* [After.js](https://github.com/jaredpalmer/after.js)
* [Gatsby](https://www.gatsbyjs.org/)

### CMS

* [Keystone](https://keystonejs.com/)