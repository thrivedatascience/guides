+++
title = "Frontend"
description = ""
weight = 1
+++

A maioria dos projetos são criados com [Create React App](https://github.com/facebookincubator/create-react-app) e geram uma estrutura que é padrão deste pacote.


{{% notice note %}}
Nosso *boilerplate* interno pode ser encontrado [aqui](https://gitlab.com/nave/react-boilerplate).
{{% /notice %}}


Com base nas diretórios gerados é usada a seguinte estrutura:

```
react-boilerplate
│   README.md
└───public
└───node_modules
└───src
        └───constants
        └───components
        └───redux
                └───modules
                        └───actions
                        └───reducers
                └───store.js
        └───services
        └───providers
        └───themes
        └───helpers
        └───screens
```


* **components**

    Todos os componentes que serão reutilizados em alguma outra parte do código, são colocados nessa pasta.

* **redux**

    Organizamos os modulos do redux e dividimos cada módulo em _action_ e _reducer_, assim gerando uma melhor legibilidade de código

* **providers**

    Onde fica configurado o nosso cliente HTTP que utilizamos para fazer as _requests_.


* **services**
  
    Todos os serviços que utilizamos no código são exportados dessa pasta e separados por entidade.

* **themes**

    Ficam todas as variavéis que são usadas como padrão na estilização.
    Exemplo: 
    ```javascript 
    export const colors = {
        primary: '#000',
        secondary: '#fff'
    }
    ```

* **helpers**

    Funções que dao suporte no desenvolvimento do projeto e que podem ser usadas em vários componentes.

* **screens**

    Pasta onde ficam todas as telas principais do projeto.
