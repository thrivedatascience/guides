+++
title = "Frontend"
description = ""
weight = 1
+++

Na parte do frontend optamos sempre em usar [React](https://reactjs.org/) como framework principal, e seguimos algumas práticas como padrão de desenvolvimento.

* Utilizar o máximo de programação funcional na criação do código.
* Biblioteca de estilização de componente.
* Manter sempre o código bem organizado

##### Desestruturar um objeto sempre que possível

{{% panel theme="warning" header="Não recomendado" %}}

        const MyComponent = props => {
            return (
                <div>{props.name}</div>
            )
        }

{{% /panel %}}

{{% panel theme="success" header="Recomendado" %}}

        const MyComponent = ({name}) => {
            return (
                <div>{name}</div>
            )
        }

{{% /panel %}}

##### Exportar o componente sempre no final do arquivo

```jsx
    class MyComponent extends Component {...}

    export default MyComponent
```

##### Componentes de classe apenas quando o mesmo tem estado

{{% panel theme="danger" header="Errado" %}}

        class MyComponent extends Component {

            render () {
                <div>meu componente</div>
            }
        }

        export default MyComponent

{{% /panel %}}

{{% panel theme="success" header="Certo" %}}

        const MyComponent = () => {
            return (
                <div>meu componente</div>
            )
        }

        export default MyComponent

{{% /panel %}}

##### Construtor

Não costumamos utilizar construtor de classe para os componentes. A explicação para isso é que o [Babel](https://babeljs.io/) já faz isso para nós.

```jsx
    class MyComponent extends Component {
        state = {
            name: 'John'
        }

        render () {
            const { name } = this.state
            return (
                <div>meu nome é: {name}</div>
            )
        }
    }

    export default MyComponent

```

##### Métodos de classe

Graças ao Babel podemos usar uma _feature_ experimental chamada _public class field_, que nos permite usar _arrow functions_ dentro da classe.

```jsx

    class MyComponent extends Component {
        myMethod = () => {...}

        render () {...}
    }

    export default MyComponent

```

##### Estilização do componente

Sempre mantemos os estilos abaixo do código do componente e sempre antes do `export` dele. E sempre lembrando de usar alguma biblioteca de css modular.

```jsx

    const MyComponent = () => {
        return (<Paragraph>Hello, world!</Paragraph>)
    }

    const Paragraph = styled.p`
        color: white;
        font-size: 1.1em;
    `

    export default MyComponent

```