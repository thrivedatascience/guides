+++
title = "Estrutura de código"
description = ""
weight = 1
+++

Na Nave usamos o padrão de código [Standard](https://standardjs.com/).

Para o Visual Studio Code existe [este plugin](https://marketplace.visualstudio.com/items?itemName=chenxsan.vscode-standardjs), garantindo que todos os programadores sigam o mesmo padrão.

{{% notice note %}}
Código é escrito **SEMPRE** em inglês.
{{% /notice %}}

## Práticas comuns

Uso de arquivos chamados `index.js` para exportar os arquivos que estão dentro da mesma pasta, assim permitindo que o programador não precisar muitos níveis ao tentar importar um código.
Essa técnica só é usada quando dentro dessa respectiva pasta contem mais de um arquivo.

{{% panel theme="warning" header="Não recomendado" %}}

```javascript
const myCode = require('./controllers/myCode')
```

{{% /panel %}}

{{% panel theme="success" header="Recomendado" %}}

```javascript
const { myCode } = require('./controllers')
```

{{% /panel %}}
