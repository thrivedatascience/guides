+++
title = "Nave"
description = ""
weight = 2
alwaysopen = true
pre ="<i class='fa fa-rocket'></i> "
+++

## Guias sobre o processo de desenvolvimento

* [Estrutura de código]({{%relref "codestructure" %}})
* [Estrutura de projeto]({{%relref "folderstructure" %}})
* [Pacotes]({{%relref "packages" %}})